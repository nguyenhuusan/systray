package net.mika.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class DbLib {
	private Connection connect() {
		String url = "jdbc:sqlite:C://var/db/mika.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	public Collection<WordMaster> selectAll() {
		String sql = "SELECT id, word, desc FROM word_master";
		Collection<WordMaster> arrList = new ArrayList<WordMaster>();

		WordMaster wm = null;
		try (Connection conn = this.connect();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {

			while (rs.next()) {
				wm = new WordMaster(rs.getInt("id"), rs.getString("word"), rs.getString("desc"));
				arrList.add(wm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return arrList;
	}

	public static void main(String[] args) {
		DbLib app = new DbLib();
		Collection<WordMaster> arrList = app.selectAll();
		for (WordMaster wm : arrList) {
			System.out.println(wm.getDesc());
		}
	}
}
