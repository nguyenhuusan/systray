package net.mika.utils;


import java.awt.*;
import java.awt.event.*;

public class SystemTrayIcon
{
    
    public SystemTrayIcon() throws Exception
    {
        
        final TrayIcon trayIcon;

        if (SystemTray.isSupported()) {

            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().getImage("tray.gif");

//            MouseListener mouseListener = new MouseListener() {
//                
//                public void mouseClicked(MouseEvent e) {
//                    System.out.println("Tray Icon - Mouse clicked!");                 
//                }
//                public void mouseEntered(MouseEvent e) {
//                    System.out.println("Tray Icon - Mouse entered!");                 
//                }
//                public void mouseExited(MouseEvent e) {
//                    System.out.println("Tray Icon - Mouse exited!");                 
//                }
//                public void mousePressed(MouseEvent e) {
//                    System.out.println("Tray Icon - Mouse pressed!");                 
//                }
//                public void mouseReleased(MouseEvent e) {
//                    System.out.println("Tray Icon - Mouse released!");                 
//                }
//
//            };

            trayIcon = new TrayIcon(image, "Tray Demo", createPopupMenu());

            ActionListener actionListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    trayIcon.displayMessage("Action Event", 
                        "An Action Event Has Been Peformed!",
                        TrayIcon.MessageType.INFO);
                }
            };
            
            trayIcon.setImageAutoSize(true);
            trayIcon.addActionListener(actionListener);
//            trayIcon.addMouseListener(mouseListener);

            //    Depending on which Mustang build you have, you may need to uncomment
            //    out the following code to check for an AWTException when you add 
            //    an image to the system tray.

            //    try {
                      tray.add(trayIcon);
            //    } catch (AWTException e) {
            //        System.err.println("TrayIcon could not be added.");
            //    }

        } else {
            System.err.println("System tray is currently not supported.");
        }
    }
    
    private PopupMenu createPopupMenu(){
    	PopupMenu popup = new PopupMenu();
    	
    	ActionListener exitListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Exiting...");
                System.exit(0);
            }
        };
        
    	MenuItem defaultItem = new MenuItem("Exit");
        defaultItem.addActionListener(exitListener);
        popup.add(defaultItem);
        
    	return popup;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try {
        	SystemTrayIcon main = new SystemTrayIcon();
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
    	
    }
    
}
