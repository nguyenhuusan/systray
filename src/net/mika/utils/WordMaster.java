package net.mika.utils;

public class WordMaster {
    private Integer id;
    private String word;
    private String desc;
	
    public WordMaster(Integer id, String word, String desc){
    	this.id = id;
    	this.word = word;
    	this.desc = desc;
    }
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
    
}
